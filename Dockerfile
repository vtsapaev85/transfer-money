FROM openjdk:11-jdk-slim
VOLUME /tmp
ARG DEPENDENCY=build/libs
COPY ${DEPENDENCY} /app
ENTRYPOINT ["java","-jar","app/gs-spring-boot-docker-0.1.0.jar"]
